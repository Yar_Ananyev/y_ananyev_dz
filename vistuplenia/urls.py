from django.conf.urls import patterns, url
from vistuplenia import views

urlpatterns = patterns('',
 url(r'^$', views.index, name='index'),\
  url(r'^pagin/', views.pagin, name='pagin'),
)
