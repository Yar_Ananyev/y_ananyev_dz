from django.shortcuts import render
from .models import news

def index(request):
    news_list = news.objects.all()
    context = {'news': news_list}
    return render(request, 'news/base_news.html', context)
