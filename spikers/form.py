from django.forms import ModelForm
from . models import spikers

class SpikForm(ModelForm):
    class Meta:
        model = spikers
        fields = ['descriptions', 'fio', 'topic', 'date']
        