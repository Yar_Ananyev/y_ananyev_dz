from django.shortcuts import render
from .models import kontakti

def index(request):
    kontakti_list = kontakti.objects.all()
    context = {'kontakti': kontakti_list}
    return render(request, 'kontakti/base_kontakti.html', context)