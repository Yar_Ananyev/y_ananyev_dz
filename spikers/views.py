from .models import spikers
from django.shortcuts import render, render_to_response, redirect
from . forms import SpikForm
from django.template import RequestContext

def index(request):
    spikers_list = spikers.objects.all()
    context = {'spikers': spikers_list}
    return render(request, 'spikers/base_spikers.html', context)
    
def change(request, p_id):
    spiker = spikers.objects.get(id = p_id)
    if request.method=='POST' :
        form = SpikForm(request.POST, instance=spiker)
        if form.is_valid():
            form.save()
            spikers_list = spikers.objects.all()
            context = {'spikers': spikers_list}
            return render(request, 'spikers/base_spikers.html', context)
    else:
        form = SpikForm(instance=spiker)
    context = {'form': form}
    return render(request, 'spikers/change.html', context)

def add(request):
    if request.method=='POST' :
        form = SpikForm(request.POST)
        if form.is_valid():
            form.save()
            spikers_list = spikers.objects.all()
            context = {'spikers': spikers_list}
            return render(request, 'spikers/base_spikers.html', context)
    else:
        form = SpikForm()
    context = {'form': form}
    return render(request, 'spikers/change.html', context)


def delete(request, p_id):
    spiker_del = spikers.objects.get(id = p_id)
    spiker_del.delete()
    spikers_list = spikers.objects.all()
    context = {'spikers': spikers_list}
    return redirect('spikers/index.html')
