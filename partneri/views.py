from django.shortcuts import render
from .models import partneri

def index(request):
    partneri_list = partneri.objects.all()
    context = {'partneri': partneri_list}
    return render(request, 'partneri/base_partneri.html', context)
