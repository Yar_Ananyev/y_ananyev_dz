from django.db import models
class news(models.Model):
 title = models.CharField(max_length=70)
 description = models.CharField(max_length=255)
 pub_date = models.DateTimeField()
 text = models.TextField()
