from django.db import models

class spikers(models.Model):
 fio = models.CharField(max_length=70)
 topic = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 date = models.DateField()

 def get_absolute_url(self):
    from django.core.urlresolvers import reverse
    return reverse('spikers.views.change', args=[str(self.id)])
 