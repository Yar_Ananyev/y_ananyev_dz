from django.conf.urls import patterns, include, url
from django.contrib import admin
from vistuplenia import views
from django.conf import settings
from django.views.generic import TemplateView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'homework.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include('news.urls')),
    url(r'^kontakti/', include('kontakti.urls')),\
    url(r'^organizatori/', include('organizatori.urls')),
    url(r'^partneri/', include('partneri.urls')),
    url(r'^spikers/', include('spikers.urls')),
    url(r'^vistuplenia/', include('vistuplenia.urls')),
    url(r'^$', views.index, name='index'),
)

if settings.DEBUG:    
    urlpatterns += patterns('', 
    url(r'^404$', TemplateView.as_view(template_name='404.html')),
    url(r'^500$', TemplateView.as_view(template_name='500.html')),)