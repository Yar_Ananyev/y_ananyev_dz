from django.shortcuts import render
from .models import organizatori

def index(request):
    organizatori_list = organizatori.objects.all()
    context = {'organizatori': organizatori_list}
    return render(request, 'organizatori/base_organizatori.html', context)
