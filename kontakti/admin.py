from django.contrib import admin

from .models import kontakti

class setadd(admin.ModelAdmin):
    list_display = ['fio']
    search_fields = ['fio']
    
admin.site.register(kontakti, setadd)
