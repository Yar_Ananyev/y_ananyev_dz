from .models import vistuplenia
from django.shortcuts import render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.cache import cache_page

@cache_page(60 * 1)
def index(request):
    vistuplenia_list = vistuplenia.objects.all()
    context = {'vistuplenia': vistuplenia_list}
    return render(request, 'vistuplenia/base_vistuplenia.html', context)
    
def pagin(request):
 from .models import vistuplenia
 vistuplenia_list = vistuplenia.objects.all()
 paginator = Paginator(vistuplenia_list, 1)
 page = request.GET.get('page')
 try:
     vistuplenia = paginator.page(page)
 except PageNotAnInteger:
     vistuplenia = paginator.page(1)
 except EmptyPage:
     vistuplenia = paginator.page(paginator.num_pages)
 context = {'vistuplenia': vistuplenia}
 return render_to_response('vistuplenia/list.html', context ,context_instance=RequestContext(request))
